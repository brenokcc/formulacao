# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase


class AppTestCase(TestCase):

    def test(self):
        User.objects.create_superuser(settings.DEFAULT_SUPERUSER, None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('', username='admin')
    def cadastrar_unidadeaquisicao(self):
        self.open('/list/formulacao/unidadeaquisicao/')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'g')
        self.click_button('Salvar')

    @testcase('', username='admin')
    def cadastrar_empresa(self):
        self.open('/list/formulacao/empresa/')
        self.click_button('Cadastrar')
        self.enter('CNPJ', '65.849.173/0001-24')
        self.enter('Nome', 'Padaria')
        self.enter('Sigla', 'pa')
        self.look_at_panel('Responsável')
        self.enter('Nome', 'Breno')
        self.enter('E-mail', 'brenokcc@yahoo.com.br')
        self.enter('Telefone Principal', '(84) 02222-2222')
        self.enter('Telefone Secundário', '')
        self.enter('Endereço', 'Centro')
        self.click_button('Salvar')

    @testcase('', username='pa')
    def cadastrar_ingrediente(self):
        self.open('/list/formulacao/ingrediente/')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Farinha de Trigo')
        self.choose('Unidade de Aquisição', 'g')
        self.enter('Peso (Kg)', '1,000')
        self.enter('Preço (R$)', '1,00')
        self.click_button('Salvar')

    @testcase('', username='pa')
    def cadastrar_formulacao(self):
        self.open('/list/formulacao/formulacao/')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Bolo')
        self.choose('Ingrediente Base', 'Farinha')
        self.click_button('Salvar')

    @testcase('', username='pa')
    def adicionar_ingredienteproduto(self):
        self.click_menu('Formulações')
        self.click_icon('Visualizar')
        self.look_at_panel('Ingredientes')
        self.click_button('Adicionar Ingrediente do Produto')
        self.look_at_popup_window()
        self.choose('Ingrediente', 'Farinha')
        self.enter('Peso (Kg)', '1,000')

    @testcase('', username='pa')
    def calcular_custo_producao(self):
        self.open('/list/formulacao/formulacao/')
        self.click_icon('Visualizar')
        self.click_button('Calcular Rendimento/Custos')
        self.look_at_popup_window()
        self.enter('Percentual de Perda (%)', '10')
        self.enter('Peso Líquido da Unidade', '1,000')
        self.enter('Percentual dos Custos (%s)', '10')
        self.click_button('Calcular Rendimento/Custos')

    @testcase('', username='pa')
    def cadastrar_ordemproducao(self):
        self.open('/list/formulacao/ordemproducao/')
        self.click_button('Cadastrar')
        self.enter('Data', '01/01/2017')
        self.choose('Produto', 'Bolo')
        self.enter('Quantidade', '10')
        self.click_button('Salvar')
