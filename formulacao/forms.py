# -*- coding: utf-8 -*-

from djangoplus.ui.components import forms
from djangoplus.decorators import action

from formulacao.models import Empresa, Formulacao


class RelatorioForm(forms.Form):
    ano = forms.ChoiceField(choices=[[2016, 2016]])
    empresa = forms.ModelChoiceField(Empresa.objects.all(), label='Empresa')
    formulacao = forms.ModelChoiceField(Formulacao.objects.all(), label='Produto', required=False)

    class Meta:
        title = 'Estatísticas'
        submit_label='Gerar Relatório'
        method = 'get'


class ImportarDadosForm(forms.Form):
    arquivo = forms.FileField(label='Arquivo')

    class Meta:
        title = 'Importar Dados'
        submit_label='Importar'

