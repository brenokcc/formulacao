# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from formulacao import views

urlpatterns = [
    url(r'^$', views.landing),
    url(r'^login/(?P<sigla>\w+)/$', views.acessar),
    url(r'^admin/$', views.index),
    url(r'', include('djangoplus.admin.urls')),
]

