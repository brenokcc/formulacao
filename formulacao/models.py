# -*- coding: utf-8 -*-

import datetime, math
from djangoplus.db import models
from djangoplus.decorators import meta, action, role
from djangoplus.utils import format_value
from djangoplus.admin.models import Unit
from django.db.models.aggregates import Sum
from decimal import Decimal
from djangoplus.utils.http import XlsResponse
from django.db.transaction import atomic


class UnidadeAquisicao(models.Model):

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    class Meta:
        verbose_name = 'Unidade de Aquisição'
        verbose_name_plural = 'Unidades de Aquisição'
        menu = 'Unidades de Aquisição'
        icon = 'fa-balance-scale'
        can_admin = 'Administrador'

    def __str__(self):
        return self.descricao


@role('codigo')
class Empresa(Unit):
    cnpj = models.CnpjField(verbose_name='CNPJ', null=True, blank=True)
    nome = models.CharField('Nome', search=True)
    codigo = models.CharField(verbose_name='Sigla', help_text='Login da empresa para acesso ao sistema.')
    nome_responsavel = models.CharField('Nome')
    email_responsavel = models.CharField('E-mail', null=True, blank=True)
    telefone_principal = models.PhoneField('Telefone Principal', null=True, blank=True)
    telefone_secundario = models.PhoneField('Telefone Secundário', null=True, blank=True)
    endereco = models.TextField('Endereço', null=True, blank=True)

    class Meta:
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'
        menu = 'Empresas'
        icon = 'fa-building'
        verbose_female = True
        can_admin = 'Administrador'
        list_display = 'cnpj', 'nome', 'codigo', 'get_total_ingredientes', 'get_total_formulacoes'

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', ('cnpj', 'codigo'))}),
        ('Responsável', {'fields': ('nome_responsavel', 'email_responsavel')}),
        ('Contato', {'fields': (('telefone_principal', 'telefone_secundario'), 'endereco')}),
        ('Ingredientes & Formulações', {'relations': ('ingrediente_set', 'formulacao_set'), 'actions' : ('importar', 'exportar')}),
    )

    def __str__(self):
        return self.nome

    @meta('Ingredientes')
    def get_total_ingredientes(self):
        return self.ingrediente_set.count()

    @meta('Formulações')
    def get_total_formulacoes(self):
        return self.formulacao_set.count()

    @action('Exportar', category=None, style='')
    def exportar(self):
        dados = []
        ingredientes = []
        for ingrediente in self.ingrediente_set.all():
            ingredientes.append((ingrediente.descricao, ingrediente.unidade_aquisicao.descricao, ingrediente.peso_unidade, ingrediente.preco))
        dados.append(('Ingredientes', ingredientes))
        for formulacao in self.formulacao_set.all():
            items = []
            for item in formulacao.ingredienteproduto_set.all():
                items.append((item.ingrediente.descricao, item.peso, 'Ingrediente'))
            for item in formulacao.ingredientecobertura_set.all():
                items.append((item.ingrediente.descricao, item.peso, 'Cobertura'))
            for item in formulacao.ingredienterecheio_set.all():
                items.append((item.ingrediente.descricao, item.peso, 'Recheio'))
            items.append((formulacao.percentual_perda, formulacao.peso_liquido_unidade, formulacao.percentual_custos))
            dados.append((formulacao.descricao, items))
        return XlsResponse(dados)

    @action('Importar', input='ImportarDadosForm')
    def importar(self, arquivo):
        import xlrd
        import os
        import tempfile
        file = tempfile.NamedTemporaryFile(delete=False)
        file.write(arquivo.read())
        file.close()
        workbook = xlrd.open_workbook(file.name)
        sheet_names = workbook.sheet_names()
        with atomic():
            formulacao = None
            for nsheet in range(0, len(sheet_names)):
                sheet = workbook.sheet_by_name(sheet_names[nsheet])
                for i in range(0, sheet.nrows):
                    if nsheet==0:
                        descricao, descricao_unidade_aquisicao, peso_unidade, preco = sheet.cell(i, 0).value, sheet.cell(i, 1).value, sheet.cell(i, 2).value, sheet.cell(i, 3).value
                        unidade_aquisicao = UnidadeAquisicao.objects.get_or_create(descricao=descricao_unidade_aquisicao)[0]
                        peso_unidade = Decimal(str(peso_unidade))
                        preco = Decimal(str(preco))
                        qs_ingrediente = Ingrediente.objects.filter(descricao=descricao, empresa=self)
                        if qs_ingrediente.exists():
                            ingrediente = qs_ingrediente[0]
                            ingrediente.peso_unidade = peso_unidade
                            ingrediente.preco = preco
                            ingrediente.save()
                        else:
                            Ingrediente.objects.get_or_create(descricao=descricao, unidade_aquisicao=unidade_aquisicao, peso_unidade=peso_unidade, preco=preco, empresa=self)
                    else:
                        if i<sheet.nrows-1:
                            descricao, peso, tipo = sheet.cell(i, 0).value, sheet.cell(i, 1).value, sheet.cell(i, 2).value
                            ingrediente = Ingrediente.objects.get(descricao=descricao.strip(), empresa=self)
                            peso = Decimal(str(peso))
                            if i==0:
                                qs_formulacao = Formulacao.objects.filter(descricao=sheet.name, empresa=self)
                                formulacao = qs_formulacao.exists() and qs_formulacao[0] or Formulacao()
                                formulacao.descricao = sheet.name
                                formulacao.empresa = self
                                formulacao.ingrediente_base = ingrediente
                                formulacao.save()
                            cls = None
                            if tipo[0]=='C':
                                cls = IngredienteCobertura
                                formulacao.requer_cobertura = True
                            elif tipo[0]=='R':
                                cls = IngredienteRecheio
                                formulacao.requer_recheio = True
                            else:
                                cls = IngredienteProduto
                            item = cls.objects.get_or_create(formulacao=formulacao, ingrediente=ingrediente, peso=peso)[0]
                            item.save()
                        else:
                            formulacao.save()
                            percentual_perda, peso_unidade, percentual_gasto_fixo = sheet.cell(i, 0).value, sheet.cell(i, 1).value, sheet.cell(i, 2).value
                            percentual_perda = Decimal(str(percentual_perda))
                            peso_unidade = Decimal(str(peso_unidade))
                            percentual_gasto_fixo = Decimal(str(percentual_gasto_fixo))
                            formulacao.calcular_custo_producao(percentual_perda, peso_unidade, percentual_gasto_fixo)

        os.unlink(file.name)


class Ingrediente(models.Model):

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    unidade_aquisicao = models.ForeignKey(UnidadeAquisicao, verbose_name='Unidade de Aquisição', filter=True)
    peso_unidade = models.DecimalField3('Peso (Kg)', help_text='Peso em kilogramas (Kg) da unidade de medida')
    preco = models.MoneyField('Preço (R$)', help_text='Valor em R$ da unidade de medida')

    empresa = models.ForeignKey(Empresa, verbose_name='Empresa', filter=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('empresa', ('descricao', 'unidade_aquisicao'), ('peso_unidade', 'preco'))}),
    )

    class Meta:
        verbose_name = 'Ingrediente'
        verbose_name_plural = 'Ingredientes'
        menu = 'Ingredientes'
        icon = 'fa-shopping-basket'
        list_lookups = 'empresa'
        can_admin = 'Empresa'

    def __str__(self):
        return self.descricao

    def preco_da_grama(self):
        return self.preco / self.peso_unidade / 1000


class Formulacao(models.Model):

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    ingrediente_base = models.ForeignKey(Ingrediente, verbose_name='Ingrediente Base', help_text='Ingrediente que servirá como base de cálculo para a formulação.')
    requer_cobertura = models.BooleanField('Requer cobertura', default=False)
    requer_recheio = models.BooleanField('Requer cobertura', default=False)

    peso_bruto = models.DecimalField3('Rendimento Bruto (Kg)', exclude=True, null=True)
    percentual_perda = models.IntegerField('Percentual de Perda (%)',  help_text='Líquido Evaporado', exclude=True, null=True)
    peso_liquido = models.DecimalField3('Rendimento Líquido (Kg)', exclude=True, null=True)
    peso_liquido_unidade = models.DecimalField3('Peso Líquido da Unidade (Kg)',  help_text='Peso em Kg da unidade a ser comercializada', exclude=True, null=True)
    unidades_produzidas = models.DecimalField('Rendimento (Unidades)',  help_text='Total de unidades produzidas', null=True, exclude=True)

    custo_total_bruto = models.DecimalField('Custo Total Bruto', exclude=True, null=True)
    custo_por_kg = models.DecimalField('Custo por Kg', exclude=True, null=True)
    custo_unitario = models.DecimalField('Custo Unitário', exclude=True, null=True)
    percentual_custos = models.IntegerField('Percentual dos Custos (%s)',  help_text='Percentual relacionado a custos fixos e variáveis e que será adicionado ao custo final de produção.', exclude=True, null=True)
    custo_total_producao = models.DecimalField('Custo Total do Produto', exclude=True, null=True)

    empresa = models.ForeignKey(Empresa, verbose_name='Empresa', filter=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('empresa', ('descricao', 'ingrediente_base',), ('requer_cobertura', 'requer_recheio'))}),
        ('Ingredientes', {'relations':('ingredienteproduto_set',)}),
        ('Recheio', {'relations': ('ingredienterecheio_set',), 'condition':'requer_recheio'}),
        ('Cobertura', {'relations': ('ingredientecobertura_set',), 'condition':'requer_cobertura'}),
        ('Rendimento', {'fields': (('peso_bruto', 'percentual_perda'), ('peso_liquido', 'peso_liquido_unidade'), 'unidades_produzidas'), 'actions' : ('calcular_custo_producao',)}),
        ('Custos da Produção', {'fields': (('custo_total_bruto', 'custo_por_kg'), ('custo_unitario', 'percentual_custos'), 'custo_total_producao')}),
    )

    class Meta:
        verbose_name = 'Formulação'
        verbose_name_plural = 'Formulações'
        menu = 'Formulações'
        icon = 'fa-list'
        add_message = 'Adicione os ingredientes'
        list_display = 'descricao', 'unidades_produzidas', 'custo_por_kg', 'custo_unitario', 'custo_total_producao',
        can_admin = 'Empresa'
        list_lookups = 'empresa'

    def __str__(self):
        return self.descricao

    @meta('Peso (g)')
    def get_peso_ingrediente_base(self):
        if self.ingredienteproduto_set.exists():
            return self.ingredienteproduto_set.all()[0].peso
        return 0

    @action('Calcular Rendimento/Custos')
    def calcular_custo_producao(self, percentual_perda, peso_liquido_unidade, percentual_custos):
        self.percentual_perda = percentual_perda
        self.peso_liquido_unidade = peso_liquido_unidade
        self.percentual_custos = percentual_custos
        self.custo_total_bruto = 0
        self.custo_total_bruto += self.ingredienteproduto_set.aggregate(Sum('custo_total')).get('custo_total__sum') or 0
        self.custo_total_bruto += self.ingredienterecheio_set.aggregate(Sum('custo_total')).get('custo_total__sum') or 0
        self.custo_total_bruto += self.ingredientecobertura_set.aggregate(Sum('custo_total')).get('custo_total__sum') or 0
        self.peso_bruto = 0
        self.peso_bruto += self.ingredienteproduto_set.aggregate(Sum('peso')).get('peso__sum') or 0
        self.peso_bruto += self.ingredienterecheio_set.aggregate(Sum('peso')).get('peso__sum') or 0
        self.peso_bruto += self.ingredientecobertura_set.aggregate(Sum('peso')).get('peso__sum') or 0
        self.peso_bruto = Decimal(self.peso_bruto)
        self.peso_liquido = self.peso_bruto - (self.peso_bruto * percentual_perda / 100)
        self.unidades_produzidas = (self.peso_liquido / peso_liquido_unidade)#.to_integral_exact(rounding=ROUND_CEILING)
        self.custo_unitario = self.custo_total_bruto / self.unidades_produzidas
        self.custo_por_kg = self.custo_total_bruto / self.peso_bruto
        self.custo_total_producao = self.custo_total_bruto + (self.custo_total_bruto * percentual_custos / 100)
        self.save()


class IngredienteFormulacao(models.Model):

    ingrediente = models.ForeignKey(Ingrediente, verbose_name='Ingrediente', composition=True)
    peso = models.DecimalField3('Peso (Kg)', help_text='Valor em kilogramas (Kg) ou litros (L)')
    percentual = models.DecimalField('Percentual (%)', exclude=True)
    preco_do_kg = models.MoneyField('Preço do Kg/L (R$)', exclude=True)
    custo_total = models.MoneyField('Custo (R$)', exclude=True)

    class Meta:
        verbose_name = 'Ingrediente para Formulação'
        verbose_name_plural = 'Ingredientes para Formulação'
        abstract = True
        ordering = 'id',

    def __str__(self):
        return self.ingrediente.descricao

    def save(self, *args, **kwargs):
        self.percentual = 100 * self.peso / (self.formulacao.get_peso_ingrediente_base() or self.peso)
        self.preco_do_kg = self.ingrediente.preco_da_grama() * 1000
        self.custo_total = self.peso * 1000 * self.ingrediente.preco_da_grama()
        super(IngredienteFormulacao, self).save()

    def calcular_producao_por_unidade(self, quantidade_unidades, peso_total, peso_ingrediente_base):
        fator_correcao = peso_total / self.formulacao.peso_bruto
        peso = self.peso * fator_correcao
        custo = self.custo_total * fator_correcao
        ingrediente = self.ingrediente
        percentual = peso_ingrediente_base and (100 * peso / peso_ingrediente_base) or 100
        return ingrediente, percentual, peso, custo


class IngredienteProduto(IngredienteFormulacao):

    formulacao = models.ForeignKey(Formulacao, verbose_name='Formulação', composition=True)

    class Meta:
        verbose_name = 'Ingrediente do Produto'
        verbose_name_plural = 'Ingredientes do Produto'
        list_display = 'ingrediente', 'peso', 'percentual', 'preco_do_kg', 'custo_total'
        can_admin = 'Empresa'
        ordering = 'id',


class IngredienteRecheio(IngredienteFormulacao):

    formulacao = models.ForeignKey(Formulacao, verbose_name='Formulação', composition=True)

    class Meta:
        verbose_name = 'Ingrediente do Recheio'
        verbose_name_plural = 'Ingredientes do Recheio'
        list_display = 'ingrediente', 'peso', 'percentual', 'preco_do_kg', 'custo_total'
        can_admin = 'Administrador', 'Empresa'
        ordering = 'id',


class IngredienteCobertura(IngredienteFormulacao):

    formulacao = models.ForeignKey(Formulacao, verbose_name='Formulação', composition=True)

    class Meta:
        verbose_name = 'Ingrediente da Cobertura'
        verbose_name_plural = 'Ingredientes da Cobertura'
        list_display = 'ingrediente', 'peso', 'percentual', 'preco_do_kg', 'custo_total'
        can_admin = 'Empresa'
        ordering = 'id',


class OrdemProducao(models.Model):

    data = models.DateField('Data', default=datetime.date.today)
    formulacao = models.ForeignKey(Formulacao, verbose_name='Produto')
    quantidade = models.IntegerField('Quantidade', help_text='Quantidade de unidades que serão produzidas')
    peso_bruto = models.DecimalField3('Peso Total Bruto (Kg)', exclude=True)
    peso_liquido = models.DecimalField3('Peso Total Líquido (Kg)', exclude=True)

    empresa = models.ForeignKey(Empresa, verbose_name='Empresa', filter=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('empresa', 'data', 'formulacao', ('quantidade', 'get_peso_unitario_liquido'), ('peso_bruto', 'get_custo_por_kg'), ('peso_liquido', 'get_custo_unitario'))}),
        ('Ingredientes do Produto', {'relations': ('ingredienteprodutoordemproducao_set',)}),
        ('Ingredientes do Recheio', {'relations': ('ingredienterecheioordemproducao_set',), 'condition':'requer_recheio'}),
        ('Ingredientes da Cobertura', {'relations': ('ingredientecoberturaordemproducao_set',), 'condition':'requer_cobertura'}),
    )

    class Meta:
        verbose_name = 'Ordem de Produção'
        verbose_name_plural = 'Ordens de Produção'
        menu = 'Ordens de Produção'
        icon = 'fa-pencil-square-o'
        add_message = 'Ingredientes calculados com sucesso'
        can_admin = 'Empresa'
        list_lookups = 'empresa'

    def __str__(self):
        return '%s - %s (QTD %s)' % (self.formulacao, format_value(self.data), self.quantidade)

    def requer_recheio(self):
        return self.ingredienterecheioordemproducao_set.exists()

    def requer_cobertura(self):
        return self.ingredientecoberturaordemproducao_set.exists()

    @meta('Peso (g)')
    def get_peso_ingrediente_base(self):
        if self.ingredienteprodutoordemproducao_set.exists():
            return self.ingredienteprodutoordemproducao_set.all()[0].quantidade
        return 0

    @meta('Peso da Unidade (Kg)')
    def get_peso_unitario_liquido(self):
        return self.formulacao.peso_liquido_unidade

    @meta('Custo Unitário (R$)')
    def get_custo_unitario(self):
        return self.formulacao.custo_unitario

    @meta('Custo por Kg (R$)')
    def get_custo_por_kg(self):
        return self.formulacao.custo_por_kg

    def save(self):
        self.peso_bruto = self.formulacao.peso_bruto * self.quantidade / self.formulacao.unidades_produzidas
        self.peso_liquido = self.formulacao.peso_liquido * self.quantidade / self.formulacao.unidades_produzidas

        super(OrdemProducao, self).save()

        IngredienteProdutoOrdemProducao.objects.filter(ordem_producao=self).delete()

        # ingrediente base
        item = self.formulacao.ingredienteproduto_set.all()[0]
        ingrediente, percentual, quantidade, custo = item.calcular_producao_por_unidade(self.quantidade, self.peso_bruto, 0)
        IngredienteProdutoOrdemProducao.objects.create(ordem_producao=self, ingrediente=ingrediente, percentual=percentual, quantidade=quantidade, custo=custo, preco_do_kg=item.preco_do_kg)

        # demais ingredientes
        peso_ingrediente_base = self.get_peso_ingrediente_base()
        for item in self.formulacao.ingredienteproduto_set.all()[1:]:
            ingrediente, percentual, quantidade, custo = item.calcular_producao_por_unidade(self.quantidade, self.peso_bruto, peso_ingrediente_base)
            IngredienteProdutoOrdemProducao.objects.create(ordem_producao=self, ingrediente=ingrediente, percentual=percentual, quantidade=quantidade, custo=custo, preco_do_kg=item.preco_do_kg)

        IngredienteRecheioOrdemProducao.objects.filter(ordem_producao=self).delete()
        for item in self.formulacao.ingredienterecheio_set.all():
            ingrediente, percentual, quantidade, custo = item.calcular_producao_por_unidade(self.quantidade, self.peso_bruto, peso_ingrediente_base)
            IngredienteRecheioOrdemProducao.objects.create(ordem_producao=self, ingrediente=ingrediente, percentual=percentual, quantidade=quantidade, custo=custo, preco_do_kg=item.preco_do_kg)

        IngredienteCoberturaOrdemProducao.objects.filter(ordem_producao=self).delete()
        for item in self.formulacao.ingredientecobertura_set.all():
            ingrediente, percentual, quantidade, custo = item.calcular_producao_por_unidade(self.quantidade, self.peso_bruto, peso_ingrediente_base)
            IngredienteCoberturaOrdemProducao.objects.create(ordem_producao=self, ingrediente=ingrediente, percentual=percentual, quantidade=quantidade, custo=custo, preco_do_kg=item.preco_do_kg)


class IngredienteOrdemProducao(models.Model):

    ingrediente = models.ForeignKey(Ingrediente, verbose_name='Ingrediente', composition=True)
    percentual = models.DecimalField('Percentual (%)')
    quantidade = models.DecimalField3('Peso (kg)')
    preco_do_kg = models.MoneyField('Preço do Kg/L (R$)', default=0)
    custo = models.MoneyField('Custo (R$)')

    class Meta:
        verbose_name = 'Ingrediente de Ordem de Produção'
        verbose_name_plural = 'Ingrediente de Ordem de Produçãos'
        abstract = True


class IngredienteProdutoOrdemProducao(IngredienteOrdemProducao):

    ordem_producao = models.ForeignKey(OrdemProducao, verbose_name='Ordem de Produção', composition=True)

    class Meta:
        verbose_name = 'Ingrediente do Produto'
        verbose_name_plural = 'Ingredientes do Produto'
        ordering = 'id',
        list_display = 'ingrediente', 'quantidade', 'percentual', 'preco_do_kg', 'custo'


class IngredienteRecheioOrdemProducao(IngredienteOrdemProducao):

    ordem_producao = models.ForeignKey(OrdemProducao, verbose_name='Ordem de Produção', composition=True)

    class Meta:
        verbose_name = 'Ingrediente do Recheio'
        verbose_name_plural = 'Ingrediente do Recheio'
        ordering = 'id',
        list_display = 'ingrediente', 'quantidade', 'percentual', 'preco_do_kg', 'custo'


class IngredienteCoberturaOrdemProducao(IngredienteOrdemProducao):

    ordem_producao = models.ForeignKey(OrdemProducao, verbose_name='Ordem de Produção', composition=True)

    class Meta:
        verbose_name = 'Ingrediente da Cobertura'
        verbose_name_plural = 'Ingredientes da Cobertura'
        ordering = 'id',
        list_display = 'ingrediente', 'quantidade', 'percentual', 'preco_do_kg', 'custo'
