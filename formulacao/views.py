# -*- coding: utf-8 -*-
from djangoplus.admin.forms import LoginForm
from djangoplus.ui.components.navigation.breadcrumbs import httprr
from formulacao.models import *
from formulacao.forms import *
from djangoplus.decorators.views import view, action


@view('Login', login_required=False)
def acessar(request, sigla):
    empresa = Empresa.objects.get(codigo=sigla)
    form = LoginForm(request, empresa)
    if form.is_valid():
        return form.submit()
    return locals()


@view('Home', login_required=False)
def landing(request):
    return locals()


@view('Principal')
def index(request):
    if request.user.has_perm('formulacao.add_empresa'):
        url = '/list/formulacao/empresa/'
    else:
        url = '/formulacao/estatisticas/'
    return httprr(request, url)


@action(OrdemProducao, 'Imprimir', style='ajax pdf')
def imprimir(request, pk):
    obj = OrdemProducao.objects.get(pk=pk)
    numero = pk.zfill(4)
    empresa = obj.empresa
    hoje = datetime.date.today()
    items = []
    items.append(('Ingredientes do Produto', obj.ingredienteprodutoordemproducao_set.all()))
    items.append(('Ingredientes do Recheio', obj.ingredienterecheioordemproducao_set.all()))
    items.append(('Ingredientes da Cobertura', obj.ingredientecoberturaordemproducao_set.all()))
    return locals()


@view('Relatório & Estatísticas', icon='fa-bar-chart-o')
def estatisticas(request):
    form = RelatorioForm(request)
    empresas = Empresa.objects.all(request.user)

    if form.is_valid():
        empresa = form.cleaned_data['empresa']
        formulacao = form.cleaned_data['formulacao']
        qs = OrdemProducao.objects.filter(empresa=empresa)
        qs_ingredientes = IngredienteProduto.objects.filter(formulacao__empresa=empresa)
        if formulacao:
            qs = qs.filter(formulacao=formulacao)
            qs_ingredientes = qs_ingredientes.filter(formulacao=formulacao)
        chart = qs.count('data').as_chart(request)
        chart1 = qs.count('data', 'empresa').as_chart(request)
        t1 = qs_ingredientes.sum('peso', 'ingrediente').as_table(request)
        t2 = qs_ingredientes.sum('custo_total', 'ingrediente').as_table()
    return locals()

